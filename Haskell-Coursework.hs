{-
READ ME: 
Fill in the function definitions for each of the questions, and uncomment them. 
The names of the functions correspond to the names given in the document cwk_15_16.pdf. 

DO NOT CHANGE THE TYPE SIGNATURES, OR THE ORDER OF THE ARGUMENTS!

You may add as many extra or helper functions as you wish, but do not make any "import" statements.
-}

-- QUESTION 1 

isReflexive :: (Eq a) => [(a,a)] -> Bool
isReflexive xs = (reflexivity (uniqueList (tupleToList xs)) xs)

isSymmetric :: (Eq a) => [(a,a)] -> Bool
isSymmetric [] = True 
isSymmetric (x:xs) 
    |elem (snd(x),fst(x)) (x:xs) = isSymmetric (removeItem (snd(x),fst(x)) xs)
    |otherwise = False 
   
isTransitive :: (Eq a) => [(a,a)] -> Bool
isTransitive xs = and [listItem (x,z) xs | (x,y) <- xs, (y,z) <- xs]

isEquivalence :: (Eq a) => [(a,a)] -> Bool
isEquivalence [] = True
isEquivalence xs
    |(isReflexive xs == True) && (isSymmetric xs == True) && (isTransitive xs == True) = True
    |otherwise = False

eqClassOf :: (Eq a) => [(a,a)] -> a -> [a]
eqClassOf [] y = []
eqClassOf (x:xs) y
    |(fst x == y) = snd x : eqClassOf xs y
    |otherwise = eqClassOf xs y

-- TEST SET FOR Q1
{-
Your functions should have the following behaviour:
isReflexive [(1,2),(2,1),(1,1),(2,2)] is True
isReflexive [(1,2),(2,1),(2,2)] is False
isSymmetric [(1,2),(2,1),(1,1),(2,2)] is True
isSymmetric [(1,2),(1,1),(2,2)] is False
isTransitive [(1,2),(2,1),(1,1),(2,2)] is True
isTransitive [(1,2),(2,3)] is False
isEquivalence [(1,2),(2,1),(1,1),(2,2)] is True
eqClassOf [(1,2),(2,1),(1,1),(2,2)] 1 is [1,2]
-}

-- QUESTION 2

multiEqual :: (Eq a) => [a] -> [a] -> Bool
multiEqual [] _ = True
multiEqual (x:xs) ys
    |length (x:xs) /= length ys = False
    |elem x ys = multiEqual xs (deleteFirst x ys)
    |otherwise = False

multiUnion :: (Eq a) => [a] -> [a] -> [a]
multiUnion xs ys = uniqueList xs ++ uniqueList ys

multiIntersection :: (Eq a) => [a] -> [a] -> [a]
multiIntersection xs ys = uniqueList(intersection xs ys)

-- TEST SET FOR Q2
{-
Your functions should have the following behaviour:
multiEqual [1,1,2] [1,2,1] is True
multiEqual [1,1,2] [1,2] is False
multiUnion [1,1,2] [1,2,2] is [1,1,2,2]
multiIntersection [1,1,2] [1,2,2] is [1,2]
-}

-- QUESTION 3

trace :: (Num a) => [[a]] -> Maybe a
trace (xs)
    |isMatrix (xs) == False = Nothing
    |(length xs == length (head xs)) == False = Nothing
    |otherwise = Just (sum (zipWith (!!) xs [0..]))

matMult3 :: (Num a) => [[a]] -> [[a]] -> [[a]]
matMult3 xs ys = [rowOneCalc xs ys ,rowTwoCalc xs ys,rowThreeCalc xs ys]

-- TEST SET FOR Q3
{-
Your functions should have the following behaviour:
trace [[1,2],[6,4]] is Just 5
matMult3 [[1,0,1],[0,1,0],[0,0,1]] [[0,1,0],[1,0,1],[1,1,0]] is
[[1,2,0],[1,0,1],[1,1,0]]
-}

-- QUESTION 4

triNumber :: Int -> Int -> [Int]  
triNumber n k = numFunc2 n k 1 n 

{-
triNumber :: Int -> Int -> [Int]
FIRST ARGUMENT IS ROW NUMBER, SECOND IS SEED/VALUE AT TIP OF TRIANGLE
-}

-- TEST SET FOR Q4
{-
Your function should have the following behaviour:
triNumber 3 1 is [2,3,5]
-}

-- QUESTION 5

combine :: Int -> Int -> (Int, Int, Int)
combine x y = tupleMaker3 ((tupleToList [euc x y]) ++ [gcd x y]) 

-- TEST SET FOR Q5
{-
Your function should have the following behaviour:
combine 3 2 is (1,-1,1)
-}

-- Question 1 resources
tupleToList :: [(a,a)] -> [a]
tupleToList ((a,b):xs) = a : b : tupleToList xs
tupleToList _          = []

reflexivity :: (Eq t1, Foldable t) => [t1] -> t (t1, t1) -> Bool
reflexivity [] _ = True
reflexivity (x:xs) ys
    |elem (x,x) ys = reflexivity xs ys
    |otherwise = False

removeItem _ [] = []
removeItem x (y:ys) 
    | x == y    = removeItem x ys
    | otherwise = y : removeItem x ys

listItem x [] = False
listItem x (y:ys)
    | x == y = True
    | otherwise = listItem x ys

-- Question 2 resources.
deleteFirst _ [] = [] 
deleteFirst a (b:bc) 
    | a == b    = bc 
    | otherwise = b : deleteFirst a bc

uniqueList :: (Eq a) => [a] -> [a]
uniqueList [] = []
uniqueList (x : xs)
    | elem x xs = uniqueList xs
    | otherwise = x: uniqueList xs

intersection :: (Eq a) => [a] -> [a] -> [a]
intersection xs ys = [z | z <- xs, elem z ys]

-- Question 3 resources.
isMatrix :: [[a]] -> Bool
isMatrix (xs) = length (uniqueList (map (length) xs)) == 1

rowOneCalc xs ys = [sum (zipWith (*) (xs !! 0) (zipWith (!!) ys [0,0,0])), sum (zipWith (*) (xs !! 0) (zipWith (!!) ys [1,1,1])), sum (zipWith (*) (xs !! 0) (zipWith (!!) ys [2,2,2]))]
rowTwoCalc xs ys = [sum (zipWith (*) (xs !! 1) (zipWith (!!) ys [0,0,0])), sum (zipWith (*) (xs !! 1) (zipWith (!!) ys [1,1,1])), sum (zipWith (*) (xs !! 1) (zipWith (!!) ys [2,2,2]))]
rowThreeCalc xs ys = [sum (zipWith (*) (xs !! 2) (zipWith (!!) ys [0,0,0])), sum (zipWith (*) (xs !! 2) (zipWith (!!) ys [1,1,1])), sum (zipWith (*) (xs !! 2) (zipWith (!!) ys [2,2,2]))]

-- Question 4 resources.
numFunc n k i j
    |i == 1 && j == 1 = k
    |i == 1 && j /= 1 = numFunc n k (j-1) (j-1) 
    |otherwise = (numFunc n k (i-1) j) + (numFunc n k (i-1) (j-1))

numFunc2 n k i j 
  | i == n = [numFunc n k n n]
  | otherwise = numFunc n k i n : numFunc2 n k (i+1) j
  
-- Question 5 resources.
tupleMaker3 :: [a] -> (a,a,a)
tupleMaker3 [x,y,z] = (x,y,z)
		   
euc :: (Integral a) => a -> a -> (a, a)
euc a b = case b of
            1 -> (0, 1)
            _ -> let (e, f) = euc b d
                 in (f, e - c*f)
  where c = a `div` b
        d = a `mod` b